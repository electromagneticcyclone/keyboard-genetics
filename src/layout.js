const { DISTANCES, EFFORTS, FINGERS, ROWS } = require("./config");

module.exports = class Layout {
  constructor(name, config) {
    this.name   = name;
    this.config = config;
  }

  toString() {
    return this.config.trim().split("\n").map((line, i) => {
      const prefix = {2: "  ", 4: "  ", 6: "   "}[i] || "";
      return i % 2 === 0 ? prefix+line.trim()+"\n" : "";
    }).join("").trim();
  }

  toCyrillic() {
    return this.toString()
        .replace(/q/g, "й")  .replace(/Q/g, "Й")
        .replace(/w/g, "ц")  .replace(/W/g, "Ц")
        .replace(/e/g, "у")  .replace(/E/g, "У")
        .replace(/r/g, "к")  .replace(/R/g, "К")
        .replace(/t/g, "е")  .replace(/T/g, "Е")
        .replace(/y/g, "н")  .replace(/Y/g, "Н")
        .replace(/u/g, "г")  .replace(/U/g, "Г")
        .replace(/i/g, "ш")  .replace(/I/g, "Ш")
        .replace(/o/g, "щ")  .replace(/O/g, "Щ")
        .replace(/p/g, "з")  .replace(/P/g, "З")
        .replace(/\[/g, "х") .replace(/\{/g, "Х") 
        .replace(/\]/g, "ъ") .replace(/\}/g, "Ъ") 
        .replace(/a/g, "ф")  .replace(/A/g, "Ф")
        .replace(/s/g, "ы")  .replace(/S/g, "Ы")
        .replace(/d/g, "в")  .replace(/D/g, "В")
        .replace(/f/g, "а")  .replace(/F/g, "А")
        .replace(/g/g, "п")  .replace(/G/g, "П")
        .replace(/h/g, "р")  .replace(/H/g, "Р")
        .replace(/j/g, "о")  .replace(/J/g, "О")
        .replace(/k/g, "л")  .replace(/K/g, "Л")
        .replace(/l/g, "д")  .replace(/L/g, "Д")
        .replace(/;/g, "ж")  .replace(/:/g, "Ж")
        .replace(/\'/g, "э") .replace(/\"/g, "Э")
        .replace(/z/g, "я")  .replace(/Z/g, "Я")
        .replace(/x/g, "ч")  .replace(/X/g, "Ч")
        .replace(/c/g, "с")  .replace(/C/g, "С")
        .replace(/v/g, "м")  .replace(/V/g, "М")
        .replace(/b/g, "и")  .replace(/B/g, "И")
        .replace(/n/g, "т")  .replace(/N/g, "Т")
        .replace(/m/g, "ь")  .replace(/M/g, "Ь")
        .replace(/,/g, "б")  .replace(/</g, "Б")
        .replace(/\./g, "ю") .replace(/>/g, "Ю") 
        .replace(/\`/g, "ё") .replace(/~/g, "Ё")
        .replace(/\//g, ".") .replace(/\?/g, ",")
        .replace(/\\т/g, "\\n");
  }

  // clean sequence in order of appearance
  toSequence() {
    return this.toString().trim().replace(/\s+/g, "").replace("\\n", "\n");
  }

  toMetrics() {
    return parse(this.config);
  }
};

// parses the layout and creates symbol -> querty letter mapping
function parse(string) {
  const keynames = Object.keys(DISTANCES);
  keynames[0] = '~'; keynames[10] = '0'; // patching the keys order
  const lines = string.trim().split("\n").map(l =>
    l.trim().split(/\s+/).map(symbol =>
      symbol === '\\n' ? '\n' : symbol
    )
  );
  const keys  = flat();

  for (let i=0; i < lines.length / 2; i++) {
    const normal_line  = lines[i * 2];
    const shifted_line = lines[i * 2 + 1];

    for (let j=0; j < normal_line.length; j++) {
      const name = keynames.shift();
      const key = {
        effort:   EFFORTS[name],
        distance: DISTANCES[name],
        finger:   FINGERS[name],
        hand:     FINGERS[name][0],
        row:      ROWS[name]
      };

      keys[normal_line[j]]  = flat(key, {shift: false});
      keys[shifted_line[j]] = flat(key, {shift: true});
    }
  }

  keys[' '] = flat({
    effort:   0,
    distance: 0,
    finger:   'thumb',
    shift:    false,
    hand:     false,
    row:      0
  });

  keys["\t"] = flat({
    effort:   0,
    distance: 0,
    finger:   'thumb',
    shift:    false,
    hand:     false,
    row:      0
  });

  keys['l-shift'] = flat({
    effort: EFFORTS['l-shift'],
    distance: DISTANCES['l-shift'],
    finger: FINGERS['l-shift'],
    shift: false,
    hand: 'l',
    row: 0
  });

  keys['r-shift'] = flat({
    effort: EFFORTS['r-shift'],
    distance: DISTANCES['r-shift'],
    finger: FINGERS['r-shift'],
    shift: false,
    hand: 'r',
    row: 0
  });

  keys['\n'].shift = false;

  return keys;
}

// creates a flat (no prototype inheritance object)
function flat(...data) {
  return Object.assign(Object.create(null), ...data);
}
