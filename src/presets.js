/**
 * Known preset layouts to measure against
 */
const Layout = require('./layout');

const presets = {
  JCUKEN: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     q w e r t y u i o p [ ] \\
     Q W E R T Y U I O P { } |
     a s d f g h j k l ; ' \\n
     A S D F G H J K L : " \\n
      z x c v b n m , . /
      Z X C V B N M < > ?
  `,
  CorpalX: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     q g m l w y f u b ; [ ] \\
     Q G M L W Y F U B : { } |
     d s t n r i a e o h ' \\n
     D S T N R I A E O H " \\n
      z x c v j k p , . /
      Z X C V J K P < > ?
  `,
  Workman: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     q d r w b j f u p ; [ ] \\
     Q D R W B J F U P : { } |
     a s h t g y n e o i ' \\n
     A S H T G Y N E O I " \\n
      z x m c v k l , . /
      Z X M C V K L < > ?
  `,
  "Workman-P": `
  \` ! @ # $ % ^ & * ( ) - =
   ~ 1 2 3 4 5 6 7 8 9 0 _ +
     q d r w b j f u p ; { } \\
     Q D R W B J F U P : [ ] |
     a s h t g y n e o i ' \\n
     A S H T G Y N E O I " \\n
      z x m c v k l , . /
      Z X M C V K L < > ?
  `,
  Colemak: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     q w f p g j l u y ; [ ] \\
     Q W F P G J L U Y : { } |
     a r s t d h n e i o ' \\n
     A R S T D H N E I O " \\n
      z x c v b k m , . /
      Z X C V B K M < > ?
  `,
  Dvorak: `
  \` 1 2 3 4 5 6 7 8 9 0 [ ]
   ~ ! @ # $ % ^ & * ( ) { }
     ' , . p y f g c r l / = \\
     " < > P Y F G C R L ? + |
     a o e u i d h t n s - \\n
     A O E U I D H T N S _ \\n
      ; q j k x b m w v z
      : Q J K X B M W V Z
  `,
  Diktor: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     w m z ] / p d r l x i o \\
     W M Z } ? P D R L X I O |
     e b t j f k y n c h q \\n
     E B T J F K Y N C H Q \\n
      a ' [ s . , v g u ;
      A " { S > < V G U :
  `,
  Diktor_VoronovMod: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     a m [ z s p d r l x i o \\
     A M { Z S P D R L X I O |
     e b t j f k y n c h q \\n
     E B T J F K Y N C H Q \\n
      / ] ' / w / v g u ;
      ? } " ? W ? V G U :
  `,
  Redaktor: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     w s z q m p l d r u i o \\
     W S Z Q M P L D R U I O |
     e b j t f k h n y c [ \\n
     E B J T F K H N Y C { \\n
      a . ' / / x v g , ;
      A > " ? ? X V G < :
  `,
  JUIYAF: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     q e b z a [ ; h / i w m \\
     Q E B Z A { : H ? I W M |
     d t f j x u n y c l , \\n
     D T F J X U N Y C L < \\n
      m ' . s o g r k p v 
      M " > S O G R K P V
  `,
  Zubachev: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     a s f z / q v h g [ w o \\
     A S F Z ? Q V H G { W O |
     u b t j e k n c y p ; \\n
     U B T J E K N C Y P : \\n
      i m . / ' , l d r x 
      I M > ? " < L D R X
  `,
  Ruhal: `
  \` 1 2 3 4 5 6 7 8 9 0 - =
   ~ ! @ # $ % ^ & * ( ) _ +
     m e b o ' w ; h r q [ ] \\
     M E B O " W : H R Q { } |
     f t j c / , d y n k p \\n
     F T J C ? < D Y N K P \\n
      s z . x a u g i l v
      S Z > X A U G I L V
  `
};

for (const name in presets) {
  exports[name] = new Layout(name, presets[name]);
}
